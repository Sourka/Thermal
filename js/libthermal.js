import { PromisedEvents } from "./PromisedEvents.js";

const fmtprops = {
	classes: {
		normal: "cl out",
		syswhite: "cl sysinfo",
		syscyan: "cl sys",
		warn: "cl warn",
		error: "cl error",
	}
};

/* Needed properties:
 * font-weight (bold)
 * font-style (cursive),
 * text-decoration: underlined, overlined, line-through
 * blink (css class animation)
 * attributes, including href and cursor: pointer, and click listeners
 * align and fixed size fields,
 * formatted number outputs (long, short, binary, hex, etc. – not necessarily)
 * font-size */
export class BasicFmt {
	#joint;
	#cssClass;

	static fmtprops = fmtprops;
	static defaults = {joint: ' ', cssClass: fmtprops.classes.normal};

	// If they will be more arguments, it will be more wise to use Object.assign().
	constructor({joint = ' ', cssClass = fmtprops.classes.normal} = this.constructor.defaults) {
		this.#joint = joint;
		this.#cssClass = cssClass;
	}

	get joint() {
		return this.#joint;
	}

	get cssClass() {
		return this.#cssClass;
	}
}

export class Fmt extends BasicFmt {}

// ----------------------- //

class Cursor {
	static styles = ["vertical", "horizontal", "block"];
	static defaults = {style: Cursor.styles[0], position: {x: 0, y: 0}};
	
	constructor({style, position} = this.constructor.defaults) {
		this.style = style;
		this.position = position; // {x, y}.
	}
}

// ----------------------- //

export class Thermal {
	#DOMPoint;
	#root;
	#output;
	#fmt;
	#cursor;
	#promisedEvents;
	
	constructor({DOM}) {
		this.#DOMPoint = DOM;
		[this.#root, this.#output] = this.constructor.buildDOM(this.#DOMPoint);
		this.#fmt = new Fmt(fmtprops.defaults);
		this.#cursor = new Cursor();
		this.#promisedEvents = new PromisedEvents(window);
		this.#initOutput();
	}

	static buildDOM(startingPoint) {
		const root = document.createElement("div");
		root.className = "thermal";
		const output = document.createElement("pre");
		output.className = "output";
		root.append(output);
		startingPoint.append(root);
		return [root, output];
	}

	static #format(strings, defaultFmt) {
		let fmt;
		if(strings[0] instanceof Fmt) {
			fmt = strings[0];
			strings.shift();
		} else {
			fmt = defaultFmt;
		}
		return {fmt, text: strings.join(fmt.joint)}
	}

	#initOutput() {
		const span = document.createElement("span");
		span.className = this.#fmt.cssClass;
		this.#output.append(span);
	}

	clear() {
		this.#output.innerHTML = "";
		this.#initOutput();
	}

	/* TODO: rebuild the method output is added to console, to allow to position the cursor correctly. */

	write(...strings) {
		const {fmt: tempFmt, text} = this.constructor.#format(strings, this.#fmt); // May modify the argument
		const span = document.createElement("span");
		span.className = tempFmt.cssClass;
		span.innerText = text;
		this.#output.append(span);
	}

	writeln(...strings) {
		strings.push('\n')
		this.write.apply(this, strings);
	}

	// TODO: Cursor positioning
	// Doc: echoing input will not create any new element nor change the format.
	// Doc: There should be always at least one „span” element.
	async getchar(echo = true, mask = "") {
		const event = await this.#promisedEvents.waitFor("keydown");
		const char = event.key.length === 1 ? event.key : String.fromCodePoint(event.keyCode);
		if(echo) {
			const spans = this.#output.children;
			const out = mask != "" ? mask : char;
			spans[spans.length - 1].innerText += out;
		}
		return char;
	}

	get DOM() {
		return this.#DOMPoint;
	}
}
