import { Thermal, Fmt } from "./libthermal.js";

const DOMRoot = document.getElementById("thermal-root");
const terminal = new Thermal({DOM: DOMRoot});

const focusify = () => DOMRoot.focus();
window.addEventListener("load", focusify);
window.addEventListener("click", focusify);

terminal.writeln("Thermal v: 0.2 | © by Sourka");

// :D
const errorFmt = new Fmt({cssClass: Fmt.fmtprops.classes.error});
while(true) {
	terminal.write(`>>> `);
	let char = '';
	let string = "";
	while(true) { // There is no „while await” construct, I need to manage it another way.
		const char = await terminal.getchar();
		if(char.codePointAt() === 13) break;
		string += char;
	}
	if(string == "") {
		continue;
	} else if(string == "clear") {
		terminal.clear();
	} else {
		terminal.writeln(errorFmt, `${string}: Command not recognized.`);
	}
}
