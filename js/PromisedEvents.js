/* Module of the JavaSen/Libre Mont library.
 * Latest revision: 23.03.2024 */

/* Defines promise–related utilities. */

// COMPAT: Modern JS syntax

// Deprecated. It creates a promise immediately.
const promisifyOld = fn =>
	new Promise((resolve, reject) => fn(resolve, reject));

const promisify = fn =>
	function() { return new Promise((resolve, reject) => fn.call(this, resolve, reject)) };

const sleep = (ms, arg) =>
	new Promise(terminate => setTimeout(() => terminate(arg), ms));


/* Run task as abortable: by timeout or abortFn.
 * Polymorphism: {executor + t} or {executor + abortFn} can be used, where „t” can be seconds or ms.
 * If abortFn is provided, time is not required. */
function makeAbortable({executor, s, ms, abortFn}) {
	const symbol = Symbol("aborted");
	abortFn = abortFn ?? ((resolve, reject) =>
		setTimeout(() => resolve(symbol), ms ?? s * 1000 ?? 1)
	);
	return async () => {
		const value = await Promise.race([
			new Promise(executor),
			new Promise(abortFn)
		]);
		if(value == symbol) {
			throw new Error("Promise aborted.");
		} else return value;
	};
}

// Not exported
const errMessages = {
	invInterface: "Given object does not provide interface for events listeners setting.",
	noSuchWaiter: "No promised event for this name on this object.",
};

const NotFoundError = Error;
const InterfaceError = Error;

/* This class helps you wait till the moment given event on the target is triggered.
 * You can set event waiter using „waitFor” method. The waiter for that event
 * will be removed after first time event is triggered. You can cancel event waiting,
 * but doing so, a promise that wait for event will reject and it's your responsibility
 * to catch an error in every place where „waitFor” was called.
 * There may be many instances of this class and many events awaited. Calling waiter
 * for the same event on the same target again will return the same Promise.
 * Once target is set at instantiation moment, it cannot be changed, due to
 * technical reasons. */
class PromisedEvents {
	#nullObj = () => Object.create(null);
	#memory = this.#nullObj(); // listeners: names and related data
	#target = null;

	// references: Object { promise, resolve, reject }
	#addListener(eventName, references) {
		this.#memory[eventName] = references;
	}

	#reject(eventName) {
		this.#memory[eventName].reject(new Error("cancelled"));
	}
	
	// Stops listening and leave data for GC
	#removeListener(eventName) {
		this.#target.removeEventListener(eventName, this.#memory[eventName].resolve);
		delete this.#memory[eventName];
	}

	constructor(eventTarget) {
		if(!eventTarget.addEventListener)
			throw new InterfaceError(errMessages.invInterface);
		this.#target = eventTarget; // target is set only once
	}

	// Does the object have „waiter”
	isWaitingFor(eventName) {
		return !!this.#memory[eventName];
	}
	
	// Returns promise, on which you can set as many .then–s as you want.
	waitFor(eventName) {
		if(this.isWaitingFor(eventName)) {
			return this.#memory[eventName].promise;
		} else {
			const data = {};
			data.promise = new Promise((resolve, reject) => {
				this.#target.addEventListener(eventName, resolve);
				data.resolve = resolve;
				data.reject = reject;
			}).finally(() => {
				if(this.isWaitingFor(eventName)) {
					this.#removeListener(eventName);
				}
			});
			// Add data packet to the registry and return the promise;
			this.#addListener(eventName, data);
			return data.promise;
		}
	}

	// Cancel/stop waiting.
	cancel(eventName) {
		if(this.isWaitingFor(eventName)) {
			this.#reject(eventName); // must be invoked before the removal
			this.#removeListener(eventName); // Do not confuse with removeEventListener
		} else throw new NotFoundError(errMessages.noSuchWaiter);
		return this; // provides ability to chain methods
	}

	cancelAll() {
	// After operation memory will be plain object again
		for(const eventName in this.#memory) {
			this.#reject(eventName); // must be invoked before the removal
			this.#removeListener(eventName); // It will remove records from this.#memory
		}
		return this;
	}
	
};

export {
	promisify, sleep, PromisedEvents, makeAbortable
};
